from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from .models import BukuTamu


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())

class Test123(TestCase):
    def test_adakah_url_formulir(self):
        response = Client().get('/formulir/')
        self.assertEquals(response.status_code, 200)

    def test_adakah_url_formulir_ada_text_BUKU_PESERTA(self):
        response = Client().get('/formulir/')
        html_return = response.content.decode('utf8')
        self.assertIn("BUKU PESERTA", html_return)
        self.assertIn("Kirim", html_return)

    def test_adakah_formulir_templatenya(self):
        response = Client().get('/formulir/')
        self.assertTemplateUsed(response, 'formulir.html')

    def test_apakah_submit_dikirim_ke_hasil(self):
        response = Client().get('/hasil/')
        self.assertEquals(response.status_code, 200)

    def test_adakah_hasil_templatenya(self):
        response = Client().get('/hasil/')
        html_return = response.content.decode('utf8')
        self.assertTemplateUsed(response, 'hasil.html')
        self.assertIn("BUKU PESERTA", html_return)

    def test_apakah_sudah_ada_model_buku_peserta(self):
        BukuTamu.objects.create(nama="INI NAMA", kegiatan="INI KEGIATAN")
        hitung_jumlah_buku = BukuTamu.objects.all().count()
        self.assertEquals(hitung_jumlah_buku, 1)

    def test_adakah_handle_post_di_halaman_hasil(self):
        response = Client().post('/hasil/',{'nama':'INI NAMA','kegiatan':'INI KEGIATAN'})
        html_return = response.content.decode('utf8')
        self.assertIn("BUKU PESERTA", html_return)
        self.assertIn("INI NAMA", html_return)
        self.assertIn("INI KEGIATAN", html_return)

    def test_adakah_url_formulir(self):
        response = self.client.get('/suatu_url/')
        self.assertEquals(response.status_code, 200)

    def test_adakah_url_formulir_ada_text_BUKU_PESERTA(self):
        response = Client().get('/suatu_url/')
        html_return = response.content.decode('utf8')
        self.assertIn("Masukkan Keyword", html_return)