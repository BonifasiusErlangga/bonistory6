from django.shortcuts import render
from django.http import HttpResponse
from .models import BukuTamu
from django.http import JsonResponse
import requests
import json

def home(request):
    return render(request, 'main/home.html')
    
def get_formulir(request):
    response ={}
    return render(request,'formulir.html', response)

def get_hasil(request):
    if request.method == 'POST':
        BukuTamu.objects.create(nama=request.POST['nama'], kegiatan=request.POST['kegiatan'])
    isi_data_buku_tamu = BukuTamu.objects.all()
    response ={'data':isi_data_buku_tamu}
    return render(request,'hasil.html', response)

def fungsi_suatu_url(request):
    response={}
    return render(request,'html_suatu_url.html', response)

def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    kembalian = requests.get(url)
    data = json.loads(kembalian.content)
    #data = {'coba': 'ini isinya', 'sesuatu': 'ini sesuatu'}
    return JsonResponse(data, safe=False)